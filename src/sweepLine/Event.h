#ifndef GEOMETRY_EVENT_H
#define GEOMETRY_EVENT_H


#include "../Figures.h"

class Event {
    Point point{};
    vector<Line> lines;
    double value;
    int type{};
public:

    Event();
    Event(const Point &point, const vector<Line> &lines, int _type );
    Event(const Point &point, const Line &line, int _type );



    void add_point(Point p) {
        point = p;
    }

    Point get_point() {
        return point;
    }

    void add_segment(Line s) {
        lines.push_back(s);
    }

    vector<Line> get_lines() {
        return lines;
    }

    void set_type(int type) {
        type = type;
    }

    int get_type() {
        return type;
    }

    void set_value(double value) {
        value = value;
    }

    double get_value() const {
        return value;
    }

    bool operator>(const Event &e) const {
        return point > e.point;

    }

};

#endif
