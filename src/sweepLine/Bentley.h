#ifndef GEOMETRY_BENTLEY_H
#define GEOMETRY_BENTLEY_H


#include "../Figures.h"
#include "Event.h"
#include <queue>
#include <set>

class Bentley {
public:
    priority_queue<Event, vector<Event>, greater<Event>> eventQ;
    set<Line, less<Line>> lineStat;
    vector<Point> results;

    Bentley(vector<Line> &inputLines) {
        for (Line &l : inputLines) {
            eventQ.push({l.start, l, 0});
            eventQ.push({l.end, l, 1});
        }
    }


    void find_intersections() {
        while (!eventQ.empty()) {
            Event evnt = eventQ.top();
            eventQ.pop();
            cout <<"Event: " << evnt.get_type() << endl;
            handleEvent(evnt);
        }
    }

    void handleEvent(Event evnt) {
        double xOfE = evnt.get_value();
        cout << evnt.get_point().x << endl;

        switch (evnt.get_type()) {
            case 0: //Line starting point
                for (Line &l : evnt.get_lines()) {
                    //recalculate(xOfE);
                    lineStat.insert(l);
                    intsectUp(lineStat, l);
                    intsectDown(lineStat, l);
                }
                break;

            case 1: //Line ending point

                for (Line &l : evnt.get_lines()) {

                    intsectUnD(lineStat, l);
//                    auto iter = lineStat.find(l);
                    std::set<Line>::iterator it = lineStat.begin();
                    while( it != lineStat.end()) {
                        it->dump();

                        if(*it == l){
                            cout<<"equ"<<endl;
                            lineStat.erase(lineStat.find(*it));
                            break;
                        }
                        it->dump();

                        it++;


                    }
                }
                break;

            case 2:
                Line l1 = evnt.get_lines().at(0);
                Line l2 = evnt.get_lines().at(1);
                swap(l1, l2);;

                intsectUp(lineStat,l2);
                intsectDown(lineStat, l1);

/*
                if (l1.start.x < l2.start.x) {

                } else {
                    intsectDown(lineStat, l2);
                    intsectUp(lineStat,l1);
                }
*/

                results.push_back(evnt.get_point());
                break;
        }
    }

    void intsectUp(set<Line> &set, Line &line) {
        if (set.lower_bound(line) != set.begin()) {
            Line up = *(--set.lower_bound(line));
            report_intersection(up, line, 1);
        }
    }

    void intsectDown(set<Line> &set, Line &line) {
        if (set.upper_bound(line) != set.end()) {
            Line down = *set.upper_bound(line);
            report_intersection(down, line, 1);
        }
    }

    void intsectUnD(set<Line> &set, Line &line) {
        if (set.lower_bound(line) != set.begin()
            && set.upper_bound(line) != set.end()) {
            Line up = *(--set.lower_bound(line));
            Line down = *set.upper_bound(line);
            report_intersection(up, down, 1);
        }
    }

    bool report_intersection(Line &l1, Line &l2, double L) {
        Point p1;
        if(l1.intersectLine(l2, p1)){
            eventQ.push({p1, {l1, l2}, 2});
            p1.dump();
            return true;

        }
//        if (p1.x > L) {
//        }
        return false;
    }

    void swap(Line &l1, Line &l2) {
        lineStat.erase(l1);
        lineStat.erase(l2);

        double tmp = l1.order;
        l1.order =l2.order;
        l2.order = tmp;

        lineStat.insert(l2);
        lineStat.insert(l1);
    }

    vector<Point> get_intersections() {
        return results;
    }

/*   void recalculate(double L) {
       for (auto &ls : lineStat) {
           ls.calculate_value(L)
       }
   }
*/
    void print_intersections() {
        printf("tema\n");

        for (Point &p : results) {
            printf("{ %.4f %.4f}\n", p.x, p.y);
        }
    }
};


#endif