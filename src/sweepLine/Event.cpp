#include "Event.h"

Event::Event() {}

Event::Event(const Point &point, const vector<Line> &lines, int _type)
        : point(point), lines(lines), value(point.x), type(_type) {}

Event::Event(const Point &point, const Line &line, int _type)
        : point(point), lines({line}), value(point.x), type(_type) {}
