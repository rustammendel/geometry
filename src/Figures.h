#ifndef GEOMETRY_FIGURES_H
#define GEOMETRY_FIGURES_H

#define logc(x) //cout << x << endl;
#define logp(x, y...) //printf(x,y)

#include <iostream>
#include <cmath>
#include <vector>
#include "quadratic.h"

using namespace std;
#define T '\t'

class Figures{
    virtual void dump() = 0;
};

#endif
