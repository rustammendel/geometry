#include "Line.h"


Line::Line(Point _start, Point _end) : start(_start), end(_end) {
    doOrder();
//    length = sqrt(pow(2.0, (end.x - start.x)) + pow(2.0, (end.y - start.y)));
//    slope = (end.y - start.y) / (end.x - start.x); //start.x * end.y - end.x * start.y;
}

bool Line::intersectLine(Line &_line, Point &ip) const {
    //p1 = (1-t)*s+t*e
    //p2 = (1-u)*z+t*f
    float t = getT(start.x, end.x, _line.start.x, _line.end.x, start.y, end.y, _line.start.y, _line.end.y);
    float u = getU(start.x, end.x, _line.start.x, _line.end.x, start.y, end.y, _line.start.y, _line.end.y);
    bool isCross = (0 <= t && t <= 1) && (0 <= u && u <= 1);
    if (isCross) {
        ip.x = (1 - t) * start.x + end.x * t;
        ip.y = (1 - t) * start.y + end.y * t;
    }
//    cout << "t: " << t << " u: " << u << endl;
    return isCross;
}

void Line::dump()  {
    cout << start.x << T << start.y << T << end.x << T << end.y << T << endl;

}

bool Line::operator<(const Line &p) const {

    return order < p.order;
//    return start.x < p.start.x || (start.x == p.start.x && start.y < p.start.x);
}

bool Line::operator==(const Line &l) const {
    return start.x == l.start.x && end.x == l.end.x
           && start.y == l.start.y && end.y == l.end.y;
}


float getU(float x1, float x2, float x3, float x4, float y1, float y2, float y3, float y4) {
    return (((x2 - x1) * (y1 - y3)) - ((y2 - y1) * (x1 - x3))) /
           (((x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4)));
}


float getT(float x1, float x2, float x3, float x4, float y1, float y2, float y3, float y4) {
    return (((x1 - x3) * (y3 - y4)) - ((y1 - y3) * (x3 - x4))) /
           (((x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4)));
}
