#include "Circle.h"


Circle::Circle(Point _center, int _radius) : center(_center), radius(_radius) {
//    cout << "circle created: " << center.x << " " << center.y << " r= " << radius << endl;
}

bool Circle::intersectCircle(Circle &_circle, Point &p1, Point &p2) const {
    double sumOfSquares = (pow((center.x - _circle.center.x), 2) +
                           pow((center.y - _circle.center.y), 2));
    double R = sqrt(sumOfSquares);      //distance between 2 centers

    if (!(abs(radius - _circle.radius) <= R
          && R <= radius + _circle.radius)
        || R == 0) { // no intersection
        return false;
    }

    double *R2 = &sumOfSquares; // same as r*r

    double a = (pow(radius, 2) - pow(_circle.radius, 2) + *R2)   //distance to radical line
               / (2 * R);
    double h = sqrt(pow(radius, 2) - pow(a, 2));    // half of the radical line

    //Point of intersection of radical line and distance between 2 centers (center of lens)
    double lx = center.x + a * (_circle.center.x - center.x) / R;
    double ly = center.y + a * (_circle.center.y - center.y) / R;

    //Rotate h by orthogonal V
    double hdy = h * (_circle.center.y - center.y) / R;
    double hdx = h * (_circle.center.x - center.x) / R;

    p1.x = lx + hdy;
    p1.y = ly - hdx;

    p2.x = lx - hdy;
    p2.y = ly + hdx;

    return true;

}

int Circle::intersectLine(Line &_line, Point &p1, Point &p2) const {
    //(x − c*x )^2 + (y − c*y )^2 = r^2
    //p(t) = (1-t)s + te
    double *x0 = &_line.start.x;
    double *x1 = &_line.end.x;
    double *y0 = &_line.start.y;
    double *y1 = &_line.end.y;
    double h = center.x;
    double k = center.y;
    double dx = *x1 - *x0;
    double dy = *y1 - *y0;

    u_int8_t isSolution = 0; //bit array for storing number of points

    double a = pow(dx, 2) + pow(dy, 2);
    double b = 2 * dx * (*x0 - h) + 2 * dy * (*y0 - k);
    double c = pow((*x0 - h), 2) +
               pow((*y0 - k), 2) -
               pow(radius, 2);

    double t1, t2;
    double delta = solveQuad(a, b, c, t1, t2);

    if (delta > 0) {

        if (0 <= t1 && t1 <= 1) {
            p1.x = (1 - t1) * _line.start.x + _line.end.x * t1;
            p1.y = (1 - t1) * _line.start.y + _line.end.y * t1;
            isSolution++;
            logp("point1: %.4f %.4f\n", p1.x, p1.y);
        }
        if (0 <= t2 && t2 <= 1) {
            p2.x = (1 - t2) * _line.start.x + _line.end.x * t2;
            p2.y = (1 - t2) * _line.start.y + _line.end.y * t2;
            isSolution += 2;
            logp("point2: %.4f %.4f\n", p2.x, p2.y);
        }

    }
    if (delta == 0 && (0 <= t1 && t1 <= 1)) {
        p1.x = (1 - t1) * _line.start.x + _line.end.x * t1;
        p1.y = (1 - t1) * _line.start.y + _line.end.y * t1;
        isSolution++;
        logp("point1: %.4f %.4f\n", p1.x, p1.y);
    }

    return isSolution;
}

void Circle::dump() {
    cout << center.x << T << center.y << T << radius << endl;

}

double Circle::getIntersectionArea(Circle &_circle) const {
    double lensArea;
    Point p1, p2;
    bool collide = intersectCircle(_circle, p1, p2);

    if (!collide || p1 == p2) {
        return 0;
    }

    double sumOfSquares = (pow((center.x - _circle.center.x), 2) +
                           pow((center.y - _circle.center.y), 2));
    double d = sqrt(sumOfSquares);      //distance between 2 centers
    int r = radius;
    int R = _circle.radius;
    if (R < r) {
        swap(r, R);
    }
    if (d <= abs(r - R) && r >= R) {
        // Return area of circle1
        return M_PI * R * R;
    }


    double ssec1 = r * r * acos((d * d + r * r - R * R) / (2 * d * r));
    double ssec2 = R * R * acos((d * d + R * R - r * r) / (2 * d * R));
    double triangle =
            0.25 * sqrt((-d + r + R) * (d + r - R) * (d - r + R) * (d + r + R)); //area of triangle with side d R r

    lensArea = ssec2 + ssec1 - (2 * triangle);
    return lensArea;
}
