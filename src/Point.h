#ifndef GEOMETRY_POINT_H
#define GEOMETRY_POINT_H


#include <iostream>
#include <cmath>
#include <vector>
#include "Figures.h"
#include "quadratic.h"

class Point: public Figures {
public:
    double x;
    double y;
    Point(){

    }
    Point(double _x,double _y):x(_x),y(_y){}

    double dp(Point _p2) const { return (x * _p2.x) + (y * _p2.y); }
    void dump() { cout << x << T << y << endl; }

    bool operator<(const Point &p) const;
    bool operator>(const Point &p) const;
    bool operator==(const Point &p) const;
    bool operator!=(const Point &p) const;
};


double crossP(const Point &O, const Point &A, const Point &B);

vector<Point> monoChain(vector<Point> &xPoints);

double shoeLace(vector<Point> &vertices);

#endif