#include "Figures.h"
#include <algorithm>
#include "Point.h"
bool Point::operator<(const Point &p) const {
    return x < p.x || (x == p.x && y < p.y);
}

bool Point::operator==(const Point &p) const {
    return p.x == x && p.y == y;
}

bool Point::operator!=(const Point &p) const {
    return !(p.x == x && p.y == y);
}

bool Point::operator>(const Point &p) const {
    return x > p.x || (x == p.x && y > p.y);
}

// (+) CCW
// (-) CW
// (0) collinear
double crossP(const Point &O, const Point &A, const Point &B) {
    return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
//    return (A.y - O.y) * (B.x - A.x) - (B.y - A.y) * (A.x - O.x); //reverse
}

vector<Point> monoChain(vector<Point> &xPoints) {
    size_t n = xPoints.size(), k = 0;
    if (n <= 3)
        return xPoints;

    vector<Point> hullPoints(2 * n);

    sort(xPoints.begin(), xPoints.end());

    for (size_t i = 0; i < n; ++i) {
        while (k >= 2 && crossP(hullPoints[k - 2], hullPoints[k - 1], xPoints[i]) <= 0)
            k--;
        hullPoints[k++] = xPoints[i];
    }

    for (size_t i = n - 1, rm = k + 1; i > 0; --i) {
        while (k >= rm && crossP(hullPoints[k - 2], hullPoints[k - 1], xPoints[i - 1]) <= 0)
            k--;
        hullPoints[k++] = xPoints[i - 1];
    }

    hullPoints.resize(k - 1);
    return hullPoints;
}

double shoeLace(vector<Point> &vertices) {
    double area = 0.0;
    int n = vertices.size();
    int j = n - 1;
    for (int i = 0; i < n; i++) {
        area += (vertices.at(j).x + vertices.at(i).x) * (vertices.at(j).y - vertices.at(i).y);
        j = i;
    }

    return abs(area / 2.0);
}