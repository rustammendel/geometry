#ifndef GEOMETRY_QUADRATIC_H
#define GEOMETRY_QUADRATIC_H

#include <iostream>
#include <cmath>

using namespace std;

double solveQuad(double a, double b, double c, double &x1, double &x2);

#endif
