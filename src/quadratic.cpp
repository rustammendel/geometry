#include "quadratic.h"

double solveQuad(double a, double b, double c, double &x1, double &x2) {
    double delta, dsq;
    delta = (b * b) - (4 * a * c);
    dsq = sqrt(delta);

    if (delta > 0) {
        x1 = (-b + dsq) / (2 * a);
        x2 = (-b - dsq) / (2 * a);
//        cout << "x1 = " << x1 << endl;
//        cout << "x2 = " << x2 << endl;
    } else if (delta == 0) {
        x1 = -b / (2 * a);
//        cout << "x1 = x2 =" << x1 << endl;
    } else {
//        cout << "No root" << endl;

    }

    return delta;
}
