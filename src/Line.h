#ifndef GEOMETRY_LINE_H
#define GEOMETRY_LINE_H

#include "Figures.h"
#include "Point.h"
#include "Line.h"


class Line : public Figures{
//    double length;
//    double slope;
public:
    Point end;
    Point start;
    double  order;

    Line(Point _start, Point _end);
    bool intersectLine(Line &_line, Point &ip) const;
    void dump() ;
    bool operator<(const Line &l) const;
    bool operator==(const Line &l) const;
    void doOrder() {
        (end.x > start.x) || (end.x == start.x && end.y > start.y) ? order = start.x : order = end.x;
    }


};

float getT(float x1, float x2, float x3, float x4, float y1, float y2, float y3, float y4);
float getU(float x1, float x2, float x3, float x4, float y1, float y2, float y3, float y4);


#endif
