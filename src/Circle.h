#ifndef GEOMETRY_CIRCLE_H
#define GEOMETRY_CIRCLE_H

#include "Figures.h"
#include "Point.h"
#include "Line.h"


class Circle : public Figures {
    Point center;
    int radius;
public:
    Circle(Point _center, int _radius);
    bool intersectCircle(Circle &_circle, Point &p1, Point &p2) const;
    int intersectLine(Line &_line, Point &p1, Point &p2) const;
    double getIntersectionArea(Circle &_circle) const;
    void dump() ;
};


#endif
