#include <fstream>
#include <istream>

#include "Figures.h"
#include "Point.h"
#include "Line.h"
#include "Circle.h"

Line readLine(istream &);
Circle readCircle(istream &);
void readFigures(istream &sin, vector<Line> &_lines, vector<Circle> &_circles);
void readFile(string &subjfile, vector<Line> &_lines, vector<Circle> &_circles);
void readConsole(vector<Line> &_lines, vector<Circle> &_circles);


int main() {
    vector<Line> lines;
    vector<Circle> circles;
    string filename = "inputs/IO.txt";

    readFile(filename, lines, circles);

//    readConsole(lines, circles);

    vector<Point> ips;
    logc (lines.size());

///Find Circle - circle intersections
    for (int i = 0; i < circles.size(); ++i) {
        for (int j = i + 1; j < circles.size(); ++j) {
            Point pi1, pi2;
            if (circles.at(i).intersectCircle(circles.at(j), pi1, pi2)) {
                ips.push_back(pi1);
                if (pi1 != pi2) { //add if not the same point
                    ips.push_back(pi2);
                }

            } else {
                //  cout << "No intersect \n";
            }
        }
    }
    logc(endl);

///Find Line - Line and Line - Circle intersections
    for (int i = 0; i < lines.size(); ++i) {
        for (int j = i + 1; j < lines.size(); ++j) {
            Point pi;
            if (lines.at(i).intersectLine(lines.at(j), pi)) {
                ips.push_back(pi);
                //cout << "intersect at: " << pi.x << " " << pi.y << endl;
            } else {
                // cout << "No intersect \n";
            }
        }
        for (auto &c: circles) {
            Point p1, p2;
            u_int8_t sol = c.intersectLine(lines.at(i), p1, p2);
            if (sol & 1) {
                ips.push_back(p1);
            }
            if (sol & 2) {
                ips.push_back(p2);
            }
        }
    }

    logc(endl);

    cout << ips.size() << endl;
    for (auto &p : ips) {
        printf("%.4f %.4f\n", p.x, p.y);
    }

    vector<Point> hullpoints = monoChain(ips);
    cout << hullpoints.size() << endl;

    for (auto &p :hullpoints) {
        printf("%.4f %.4f\n", p.x, p.y);
    }

    printf("%.4f\n", shoeLace(hullpoints));


    ///Find area of lens:
    Circle ca1 = readCircle(cin);
    Circle ca2= readCircle(cin);
    cout << ca1.getIntersectionArea(ca2);


    return 0;
}


Circle readCircle(istream &sin) {
    float x, y;
    int c;
    sin >> x >> y >> c;

    return {{x, y}, c};
}

Line readLine(istream &sin) {
    float x0, y0, x1, y1;
    sin >> x0 >> y0 >> x1 >> y1;

    return {{x0, y0},
            {x1, y1}};
}

void readFigures(istream &sin, vector<Line> &_lines, vector<Circle> &_circles) {
    int n;
    sin >> n;

    while (n > 0) {
        char fig;
        sin >> fig;

        if (fig == 'L') {
            _lines.push_back(readLine(sin));
        } else if (fig == 'C') {
            _circles.push_back(readCircle(sin));
        } else {
            cout << "ERR: Unrecognized figure!\n";
        }
        n--;

    }

}

void readConsole(vector<Line> &_lines, vector<Circle> &_circles) {
    readFigures(cin, _lines, _circles);
}

void readFile(string &subjfile, vector<Line> &_lines, vector<Circle> &_circles) {
    ifstream fin(subjfile);
    if (fin.is_open()) {
        readFigures(fin, _lines, _circles);
        fin.close();
    }
}

/*
8
C 0 0 5
L -2 1 12 6
C 3 2 7
C 3 10 1
L -4 9 8 -4
L 5 2 7 9
C 8 2 2
L -3 -3 -1 2

2
C 0 0 2
C 0 0 2

2
L 0 0 4 0
L 4 0 6 0

L 0 9 3 9

4
C 0 50 500
C -100 100 1
C -100 -100 1
C 100 -100 1


2
L -3 3 6 0
C 2 2 3

*/

/*
15
1.3720 -4.8081
-3.9105 3.1158
3.0000 9.0000
10.0000 2.0000
2.0496 2.4463
5.4773 3.6705
-1.3000 1.2500
3.9135 3.1120
9.3064 5.0380
4.9512 -0.6971
-0.2994 4.9910
7.4496 -3.4038
-2.0311 6.8670
6.6981 7.9434
-2.6519 -2.1298
9
-3.9105 3.1158
-2.6519 -2.1298
1.3720 -4.8081
7.4496 -3.4038
10.0000 2.0000
9.3064 5.0380
6.6981 7.9434
3.0000 9.0000
-2.0311 6.8670
140.0776
 */